## 项目已经废弃，纯前端验证没有任何意义，php项目可以使用这个前后端双重验证 [点选式后端验证码](https://gitee.com/ovsexia/captcha)

### 仿B站登录图形拖动验证码

#### 预览地址
[https://ovsexia.gitee.io/imgver](https://ovsexia.gitee.io/imgver)


#### 介绍
1. 本地验证
2. 支持PC、手机版
3. 支持多语言，目前有中、英两种语言

------
2.0新增php后台验证

------

2020-07-04:

经证实无法做到后端验证，已改用其他验证方案 [点选式后端验证码](https://gitee.com/ovsexia/captcha)